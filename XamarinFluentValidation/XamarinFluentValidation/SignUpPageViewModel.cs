﻿using System;
using System.Threading.Tasks;
using Acr.UserDialogs;
using FluentValidation;
using PropertyChanged;
using Xamarin.Forms;

namespace XamarinFluentValidation
{
    [ImplementPropertyChanged]
    public class SignUpPageViewModel
    {
        private readonly IValidator<SignUpPageViewModel> _validator;

        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public SignUpPageViewModel()
        {
            _validator = new SignUpPageValidator();
            DateOfBirth = DateTime.Today;
        }

        private Command signUpCommand;

        public Command SignUpCommand
        {
            get
            {
                return signUpCommand ?? (signUpCommand =
                  new Command(async () => await ExecuteSignUpCommand()));
            }
        }

        protected async Task ExecuteSignUpCommand()
        {
            var validationResult = _validator.Validate(this);

            if (validationResult.IsValid)
            {
                UserDialogs.Instance.ShowLoading("Validation Success...");
            }
            else
            {
                UserDialogs.Instance.ShowError(validationResult.Errors[0].ErrorMessage, 3000);
            }
        }

        private Command cancelLoginCommand;

        public Command CancelLoginCommand
        {
            get
            {
                return cancelLoginCommand ?? (cancelLoginCommand =
                  new Command(async () => await ExecuteCancelLoginCommand()));
            }
        }

        protected async Task ExecuteCancelLoginCommand()
        {

        }
    }
}
 