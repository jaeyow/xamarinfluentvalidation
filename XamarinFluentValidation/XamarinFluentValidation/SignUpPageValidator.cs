﻿using System;
using FluentValidation;

namespace XamarinFluentValidation
{
    public class SignUpPageValidator : AbstractValidator<SignUpPageViewModel>
    {
        private const int ThirteenYearsOld = 4745;
        private const int HundredYearsOld = 36500;

        public SignUpPageValidator()
        {
            RuleFor(signupUser => signupUser.Firstname)
                .NotEmpty()
                .Length(1, 100)
                .Matches(@"^[A-Za-z0-9_ ]{1,100}$");

            RuleFor(signupUser => signupUser.Lastname)
                .NotEmpty()
                .Length(1, 100)
                .Matches(@"^[A-Za-z0-9_ ]{1,100}$");

            RuleFor(signupUser => signupUser.Email)
                .NotEmpty()
                .Length(3, 50)
                .EmailAddress();

            RuleFor(signupUser => signupUser.DateOfBirth)
                .NotEmpty()
                .Must(BeWithinRange)
                .WithMessage("You must be 13 years old or over");

            RuleFor(signupUser => signupUser.Password)
                .NotEmpty()
                .Length(6, 50);

            RuleFor(signupUser => signupUser.ConfirmPassword)
                .NotEmpty()
                .Length(6, 50)
                .Equal(x => x.Password)
                .WithMessage("Passwords do not match");
        }

        private bool BeWithinRange(DateTime dateOfBirth)
        {
            var age = DateTime.Today - dateOfBirth;

            return (age.TotalDays >= ThirteenYearsOld && age.TotalDays <= HundredYearsOld);
        }
    }
}
