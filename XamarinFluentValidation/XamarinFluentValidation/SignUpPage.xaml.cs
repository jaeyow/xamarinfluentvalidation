﻿using Xamarin.Forms;

namespace XamarinFluentValidation
{
    public partial class SignUpPage : ContentPage
    {
        public SignUpPage()
        {
            BindingContext = new SignUpPageViewModel();
            InitializeComponent();
        }
    }
}
