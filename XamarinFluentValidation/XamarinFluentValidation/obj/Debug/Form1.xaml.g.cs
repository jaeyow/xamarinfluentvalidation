//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace mPortApp.Views.Initial {
    using System;
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;
    
    
    public partial class SignUpmPortPage : ContentPage {
        
        private Entry FirstNameEntry;
        
        private Entry LastNameEntry;
        
        private Entry EmailEntry;
        
        private DatePicker DateOfBirth;
        
        private Entry PasswordEntry;
        
        private Entry ConfirmPasswordEntry;
        
        private void InitializeComponent() {
            this.LoadFromXaml(typeof(SignUpmPortPage));
            FirstNameEntry = this.FindByName<Entry>("FirstNameEntry");
            LastNameEntry = this.FindByName<Entry>("LastNameEntry");
            EmailEntry = this.FindByName<Entry>("EmailEntry");
            DateOfBirth = this.FindByName<DatePicker>("DateOfBirth");
            PasswordEntry = this.FindByName<Entry>("PasswordEntry");
            ConfirmPasswordEntry = this.FindByName<Entry>("ConfirmPasswordEntry");
        }
    }
}
